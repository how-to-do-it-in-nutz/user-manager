# 简介

这是一个maven工程, eclipse/idea均可按maven项目导入

MainLauncher是入口,启动即可

本项目是忠于 https://gitee.com/how-to-do-it/user-manager/issues 所列需求

具体写法不太"nutz",但功能是没问题的^_^

## 环境要求

* 必须JDK8+
* eclipse或idea等IDE开发工具,可选

## 配置信息位置

数据库配置信息,jetty端口等配置信息,均位于src/main/resources/application.properties

## 命令下启动

仅供测试用,使用mvn命令即可

```
// for windows
set MAVEN_OPTS="-Dfile.encoding=UTF-8"
mvn compile nutzboot:run

// for *uix
export MAVEN_OPTS="-Dfile.encoding=UTF-8"
mvn compile nutzboot:run
```

## 打包成发布文件

打包成独立运行的jar文件

```
mvn clean package nutzboot:shade
```

打包成war, 仅web项目
```
mvn clean package nutzboot:shade nutzboot:war
```

运行前调整参数,例如调整端口

```
java -Dserver.port=12002 -jar xxx.jar
```
## 相关资源
论坛: https://nutz.cn
官网: https://nutz.io
一键生成NB的项目: https://get.nutz.io
