// var URL = "http://localhost/api/v1";

var URL="/api/v1";

function api(path, method, data, success) {
    $.ajax({
        url: URL + path,
        type: method,
        data: data,
        success: function (data, textStatus, jqXHR) {
            success(data, textStatus, jqXHR);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            var index = layer.alert(XMLHttpRequest.responseText, function () {
                if (XMLHttpRequest.status == 403) {
                    location.href = "/login.html";
                }
                layer.close(index)
            });
        }
    });
}

function getUrlParam(name) {
    var reg = new RegExp("(^|\\?|&)" + name + "=([^&]*)(\\s|&|$)", "i");
    if (reg.test(location.href))
        return unescape(RegExp.$2.replace(/\+/g, " "));
    return "";
}