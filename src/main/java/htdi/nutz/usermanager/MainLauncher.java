package htdi.nutz.usermanager;

import org.nutz.boot.NbApp;
import org.nutz.dao.Dao;
import org.nutz.dao.util.Daos;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.adaptor.WhaleAdaptor;
import org.nutz.mvc.annotation.AdaptBy;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Fail;
import org.nutz.mvc.annotation.Ok;

import htdi.nutz.usermanager.bean.User;

@IocBean(create = "init", depose = "depose")
@Fail("http:500")
@AdaptBy(type = WhaleAdaptor.class)
public class MainLauncher {

    @Inject
    protected Dao dao;

    @At("/")
    @Ok("->:/login.html")
    public void index() {}

    public void init() {
        // NB自身初始化完成后会调用这个方法
        Daos.createTablesInPackage(dao, User.class, false);
    }

    public void depose() {}

    public static void main(String[] args) throws Exception {
        new NbApp().setArgs(args).setPrintProcDoc(true).run();
    }

}
